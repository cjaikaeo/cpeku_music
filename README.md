# CPE-KU Music Plugin for KidBright

ปลั๊กอินสำหรับ KidBright IDE เพื่อสั่งงานให้อุปกรณ์ KidBright เล่นทำนองเพลงในรูปแบบของ
[Music Macro Language (MML)](https://en.wikipedia.org/wiki/Music_Macro_Language)
ซึ่งแต่ก่อนนิยมใช้กันอย่างแพร่หลายในการเขียนโปรแกรมด้วย[ภาษาเบสิก](https://www.msx.org/wiki/PLAY)

## การติดตั้ง 

### วิธีที่ 1
1. ดาวน์โหลดไฟล์ .zip จากลิงก์ https://ecourse.cpe.ku.ac.th/download/cpeku_music.zip
2. เข้าสู่โปรแกรม KidBright เลือกเมนู Plugins &rarr; Install Plugins แล้วเลือกไฟล์ `cpeku_music.zip` ที่บันทึกเอาไว้


### วิธีที่ 2
1. ดาวน์โหลดไฟล์ .zip จากลิงก์ https://gitlab.com/cjaikaeo/cpeku_music/-/archive/master/cpeku_music-master.zip
   เมื่อแตกไฟล์ออกจะได้โฟลเดอร์ชื่อ `cpeku_music-master`
2. เปลี่ยนชื่อ `cpeku_music-master` ให้เป็น `cpeku_music` และนำไปวางไว้ในโฟลเดอร์ `plugins` ของ KidBright IDE


## บล็อกที่รองรับ

|  **บล็อก (ภาษาอังกฤษ)**  |  **บล็อก (ภาษาไทย)**  | **การทำงาน** |
| ---------------------- | -------------------- | ------------ |
| <img src="img/play-en.png"> | <img src="img/play-th.png"> | เล่นทำนองเพลงที่ระบุในรูปแบบ MML |
| <img src="img/is_playing-en.png"> | <img src="img/is_playing-th.png"> | ตรวจสอบว่าเพลงยังบรรเลงอยู่หรือไม่ |
| <img src="img/wait_done-en.png"> | <img src="img/wait_done-th.png"> | รอจนกระทั่งเพลงบรรเลงแล้วเสร็จจึงทำงานต่อ |
| <img src="img/pause-en.png"> | <img src="img/pause-th.png"> | หยุดเล่นดนตรีชั่วขณะ |
| <img src="img/resume-en.png"> | <img src="img/resume-th.png"> | เล่นดนตรีต่อจากที่หยุดเอาไว้ |
| <img src="img/cancel-en.png"> | <img src="img/cancel-th.png"> | หยุดและยกเลิกการเล่นโน้ตทั้งหมดที่เหลืออยู่ |


## ตัวอย่างการใช้งาน

โปรแกรมดังภาพรอกดปุ่ม Switch 1 เมื่อปล่อยแล้วอุปกรณ์ KidBright จะเริ่มบรรเลงเพลง Happy
Birthday ทางลำโพง

#### (บล็อกภาษาอังกฤษ)
<img src="img/example-en.png">

#### (บล็อกภาษาไทย)
<img src="img/example-th.png">


## ตัวอย่างเพลง

เนื่องจาก MML เป็นมาตรฐานที่ใช้กันแพร่หลาย จึงสามารถหาตัวอย่างเพลงในรูปของ MML ได้จากอินเทอร์เน็ต ตัวอย่างเช่น https://archeagemmllibrary.com/

### เพลงชาติไทย
```
t100o4
g16>c8.e16g4g8.g16a8.g16f8.a16g2e4
e8.f16e4d4p8.d16d8.e16d8.e16c2
c8.e16g4g8.g16a8.g16f8.a16g2.
g8.a16b8.g16>d2<a8.b16a2g4
g8.a16g8.f16d4p4 f8.a16g8e8c2
c8.e16d4d8.d16a4b8.a16g2&g8.
c16c8.e16g4g4a8.b16>c8.d16e2&e8.
d16c8.<a16g8.a16b8.>c16e8.c16d8.c16c2&c8
```

### Happy Birthday To You
```
t120o4
g8.g16 a4g4>c4 <b2g8.g16a4g4>d4c2<g8.g16>g4e4c4<b4a4&a2.>f8.f16e4c4d4c1p2.
```

### Fur Elise
(ดัดแปลงจาก https://archeagemmllibrary.com/beethoven-fur-elise-2/)
```
t65o5
l16>ed+ed+ec-dc<a8rceab8reg+bb+8re>ed+ed+ec-dc<a8rceab8rdb+ba8rb>
cde8.<g>fed8.<f>edc8.<e>dc<br8r>er8rer8d+er8d+ed+ed+ec-dc<a8rceab8reg+
bb+8re>ed+ed+ec-dc<a8rceab8rt40db+t20ba8
```

## ข้อจำกัด
* รองรับการเล่นโน้ตเพียง 1 ช่องสัญญาณ
* ยังไม่รองรับการเล่นโน้ตแบบ triplet
* ไม่รองรับคำสั่ง `X` และเหตุการณ์ที่กำหนดขึ้นเอง
* ทำนองเพลงจะบรรเลงหลังฉากเสมอ โดยที่คำสั่ง `MB` และ `MF` ไม่มีผลใด ๆ ต่อการทำงาน


## ผู้พัฒนา

ผศ.ดร.ชัยพร ใจแก้ว ภาควิชาวิศวกรรมคอมพิวเตอร์ คณะวิศวกรรมศาสตร์ มหาวิทยาลัยเกษตรศาสตร์



