Blockly.JavaScript['cpeku_music_play'] = function(block) {
  var value_music = Blockly.JavaScript.valueToCode(block, 'music', Blockly.JavaScript.ORDER_ATOMIC);
  var code = 'DEV_IO.CPEKU_Music().play(' + value_music + ');\n';
  return code;
};

Blockly.JavaScript['cpeku_music_is_playing'] = function(block) {
  var code = 'DEV_IO.CPEKU_Music().is_playing()';
  return [code, Blockly.JavaScript.ORDER_ATOMIC];
};

Blockly.JavaScript['cpeku_music_wait_until_done'] = function(block) {
  var code = 'DEV_IO.CPEKU_Music().wait_done();\n';
  return code;
};

Blockly.JavaScript['cpeku_music_pause'] = function(block) {
  var code = 'DEV_IO.CPEKU_Music().pause();\n';
  return code;
};

Blockly.JavaScript['cpeku_music_resume'] = function(block) {
  var code = 'DEV_IO.CPEKU_Music().resume();\n';
  return code;
};

Blockly.JavaScript['cpeku_music_cancel'] = function(block) {
  var code = 'DEV_IO.CPEKU_Music().cancel();\n';
  return code;
};
