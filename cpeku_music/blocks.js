Blockly.Blocks['cpeku_music_play'] = {
  init: function() {
    this.appendValueInput("music")
        .setCheck("String")
        .appendField(Blockly.Msg.CPEKU_MUSIC_PLAY);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(270);
 this.setTooltip(Blockly.Msg.CPEKU_MUSIC_PLAY_TOOLTIP);
 this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_music_is_playing'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MUSIC_IS_PLAYING);
    this.setOutput(true, "Boolean");
    this.setColour(270);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_music_wait_until_done'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MUSIC_WAIT_DONE);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(270);
    this.setTooltip("");
    this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_music_pause'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MUSIC_PAUSE);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(270);
 this.setTooltip(Blockly.Msg.CPEKU_MUSIC_PAUSE_TOOLTIP);
 this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_music_resume'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MUSIC_RESUME);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(270);
 this.setTooltip(Blockly.Msg.CPEKU_MUSIC_RESUME_TOOLTIP);
 this.setHelpUrl("");
  }
};

Blockly.Blocks['cpeku_music_cancel'] = {
  init: function() {
    this.appendDummyInput()
        .appendField(Blockly.Msg.CPEKU_MUSIC_CANCEL);
    this.setPreviousStatement(true, null);
    this.setNextStatement(true, null);
    this.setColour(270);
 this.setTooltip(Blockly.Msg.CPEKU_MUSIC_CANCEL_TOOLTIP);
 this.setHelpUrl("");
  }
};
