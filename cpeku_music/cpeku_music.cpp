#include <map>
#include <iostream>
#include <cstdlib>
#include <cstring>

#define LOG_LOCAL_LEVEL ESP_LOG_NONE
//#define LOG_LOCAL_LEVEL ESP_LOG_INFO
#include "esp_log.h"
#include "xtensa/core-macros.h"
#include "kidbright32.h"
#include "sound.h"
#include "cpeku_music.h"
#include "pt/pt.h"

#define MILLIS() (xTaskGetTickCount()*portTICK_RATE_MS)

static const char *TAG = "CPEKU_Music";
extern SOUND sound;
static uint16_t NOTE_FREQS[] = {
 // C     C#    D     D#    E     F     F#    G     G#    A     A#    B
   16  , 17  , 18  , 19  , 21  , 22  , 23  , 24  , 26  , 28  , 29  , 31  ,
   33  , 35  , 37  , 39  , 41  , 44  , 46  , 49  , 52  , 55  , 58  , 62  ,
   65  , 69  , 73  , 78  , 82  , 87  , 92  , 98  , 104 , 110 , 117 , 123 ,
   131 , 139 , 147 , 156 , 165 , 175 , 185 , 196 , 208 , 220 , 233 , 247 ,
   262 , 277 , 294 , 311 , 330 , 349 , 370 , 392 , 415 , 440 , 466 , 494 ,
   523 , 554 , 587 , 622 , 659 , 698 , 740 , 784 , 831 , 880 , 932 , 988 ,
   1046, 1109, 1175, 1245, 1319, 1397, 1480, 1568, 1661, 1760, 1865, 1976
};

CPEKU_Music::state_t CPEKU_Music::_state;
bool CPEKU_Music::_paused;
CPEKU_MMLParser CPEKU_Music::_mmlparser;
CPEKU_MMLParser::notebuf_t CPEKU_Music::_notebuf;
pt pt_play;

CPEKU_MMLParser::CPEKU_MMLParser() {
  _note_map = { {'C',0},{'D',2},{'E',4},{'F',5},{'G',7},{'A',9},{'B',11} };
}

static const char* extract_digits(const char* s, int* pvalue) {
  uint8_t len = 0;
  *pvalue = 0;
  while (std::isdigit(*s)) {
    *pvalue = (*s - '0') + 10*(*pvalue);
    s++;
  }
  return s;
}

uint16_t CPEKU_MMLParser::parse(const char* seq, notebuf_t& notes) {
  uint16_t count = 0;
  const char* pos = seq;
  char current;
  int prev_note_num = -1;
  int prev_length = -1;
  int num = 0, divide;
  double length;

  while (*pos) {
    char upper = std::toupper(*pos);
    if (std::strchr("ABCDEFGPR",upper)) { // a note or a rest
      if (upper != 'P' && upper != 'R') { // a note
        num = _note_map[upper] + 12*_octave;
        pos++;
        if (*pos == '+' || *pos == '#') {
          num++; pos++;
        }
        else if (*pos == '-') {
          num--; pos++;
        }
      }
      else { // a rest
        num = 0; pos++;
      }
      pos = extract_digits(pos, &divide);
      if (divide == 0)
        divide = _divide;
      length = 60.0*4/_tempo/divide;
      if (*pos == '.') {
        length *= 1.5;
        pos++;
      }
      if (prev_note_num != -1) { // tied to the previous note
        if (prev_note_num != num) {
          //throw "Tied notes cannot be different";
        }
        length += prev_length;
      }
      // check for possible following note tie
      if (*pos == '&') {
        prev_note_num = num;
        prev_length = length;
        pos++;
      }
      else {
        prev_note_num = -1;
        prev_length = -1;
        double hold = _hold_duration(length);
        notes.push(note_t(num,hold,_vel));
        notes.push(note_t(0,length-hold,0));
        count += 2;
      }
    }
    else if (upper == '<') { // octave down
      _octave--; pos++;
    }
    else if (upper == '>') { // octave up
      _octave++; pos++;
    }
    else if (std::strchr("LNOVT",upper)) {
      int value;
      pos++;
      pos = extract_digits(pos, &value);
      if (upper == 'L' && value > 0)  // default length
        _divide = value;
      else if (upper == 'N') { // note number
        length = 60.0*4/_tempo/_divide;
        double hold = _hold_duration(length);
        notes.push(note_t(num,hold,_vel));
        notes.push(note_t(0,length-hold,0));
        count += 2;
      }
      else if (upper == 'O' && value > 0) // default octave
        _octave = value;
      else if (upper == 'V') // velocity (can be zero?)
        _vel = value;
      else if (upper == 'T' && value > 0) // tempo
        _tempo = value;
    }
    else if (upper == 'M') {   // music mode
      pos++;
      upper = std::toupper(*pos);
      // normal, legato, or staccato style
      if (upper == 'N' || upper == 'L' || upper == 'S') {
        _mode = std::tolower(upper);
        pos++;
      }
      // foreground or background music (not supported)
      else if (upper == 'B' || upper == 'F') {
        pos++;
      }
    }
    else // keep on looking
      pos++;
  }
  return count;
}

double CPEKU_MMLParser::_hold_duration(double duration) {
  // Compute the note-on interval for the given duration and current
  // mode of playing (e.g., staccato, legato)
  double hold;
  if (_mode == 's') // staccato
    hold = std::min(0.1,duration);
  else if (_mode == 'l') // legato
    hold = duration;
  else  // normal
    hold = duration - std::min(0.1,duration/4.0);
  return hold;
}

CPEKU_Music::CPEKU_Music() {
  _state = DETECT;
  _paused = false;
  PT_INIT(&pt_play);
}

void CPEKU_Music::init(void) {
  _state = DETECT;
  esp_log_level_set(TAG,LOG_LOCAL_LEVEL);
}

void CPEKU_Music::process(Driver *drv) {
  switch (_state) {
    case DETECT:
      // clear error flag
      error = false;
      // set initialized flag
      initialized = true;
      _state = RUNNING;
      break;
    case RUNNING:
      _task_play(&pt_play);
      break;
  }
}

PT_THREAD(CPEKU_Music::_task_play(struct pt* pt)) {

  PT_BEGIN(pt);

  static uint32_t ts;
  static uint32_t duration_ms;
  for (;;) {
    sound.off();
    PT_WAIT_WHILE(pt, _paused || _notebuf.empty());
    auto note = _notebuf.front();
    _notebuf.pop();
    if (note.freq > 0) {
      ESP_LOGI(TAG, "play freq %d for %g seconds with velocity %d (%d notes remaining)",
          NOTE_FREQS[note.freq], note.duration, note.velocity, _notebuf.size());
      int duty = note.velocity*50/127;
      if (duty > 50) duty = 50;
      sound.on(NOTE_FREQS[note.freq],duty);
    }
    else {
      ESP_LOGI(TAG, "pause for %g seconds", note.duration);
    }
    ts = MILLIS();
    duration_ms = (uint32_t)(note.duration*1000);
    PT_WAIT_UNTIL(pt, MILLIS()-ts >= duration_ms);
  }

  PT_END(pt);
}

void CPEKU_Music::play(const char* seq) {
  uint16_t count = _mmlparser.parse(seq, _notebuf);
  ESP_LOGI(TAG, "add %d notes to buffer", count);
}

bool CPEKU_Music::is_playing() {
  return !_notebuf.empty();
}

void CPEKU_Music::wait_done() {
  while (!_notebuf.empty()) {
    vTaskDelay(50 / portTICK_RATE_MS);
  }
}

void CPEKU_Music::pause() {
  _paused = true;
}

void CPEKU_Music::resume() {
  _paused = false;
}

void CPEKU_Music::cancel() {
  sound.off();
  while (!_notebuf.empty())
    _notebuf.pop();
}
