Blockly.Msg.CPEKU_MUSIC_PLAY = "เล่นดนตรี";
Blockly.Msg.CPEKU_MUSIC_PLAY_TOOLTIP = "เล่นดนตรีตามโน้ตที่ระบุในรูป Music Macro Language (MML)";

Blockly.Msg.CPEKU_MUSIC_IS_PLAYING = "ดนตรีกำลังเล่น";
Blockly.Msg.CPEKU_MUSIC_IS_PLAYING_TOOLTIP = "ตรวจสอบว่าดนตรีกำลังอยู่ระหว่างการบรรเลงหรือไม่";

Blockly.Msg.CPEKU_MUSIC_WAIT_DONE = "รอจนกระทั่งดนตรีเล่นเสร็จ";
Blockly.Msg.CPEKU_MUSIC_WAIT_DONE_TOOLTIP = "หยุดรอจนกระทั่งดนตรีเล่นโน้ตที่ค้างไว้เสร็จสิ้น";

Blockly.Msg.CPEKU_MUSIC_PAUSE = "หยุดเล่นดนตรีชั่วขณะ";
Blockly.Msg.CPEKU_MUSIC_PAUSE_TOOLTIP = "หยุดเล่นชั่วขณะ";

Blockly.Msg.CPEKU_MUSIC_RESUME = "เล่นดนตรีต่อ";
Blockly.Msg.CPEKU_MUSIC_RESUME_TOOLTIP = "เล่นดนตรีต่อจากที่หยุดค้างไว้";

Blockly.Msg.CPEKU_MUSIC_CANCEL = "ยกเลิกดนตรี";
Blockly.Msg.CPEKU_MUSIC_CANCEL_TOOLTIP = "หยุดดนตรีและยกเลิกโน้ตที่เล่นค้างไว้ทั้งหมด";
