Blockly.Msg.CPEKU_MUSIC_PLAY = "play music";
Blockly.Msg.CPEKU_MUSIC_PLAY_TOOLTIP = "play music from sequence based on Music Macro Language (MML)";

Blockly.Msg.CPEKU_MUSIC_IS_PLAYING = "music is playing";
Blockly.Msg.CPEKU_MUSIC_IS_PLAYING_TOOLTIP = "check if music is currently playing";

Blockly.Msg.CPEKU_MUSIC_WAIT_DONE = "wait until music is done";
Blockly.Msg.CPEKU_MUSIC_WAIT_DONE_TOOLTIP = "wait (block) until music is done playing";

Blockly.Msg.CPEKU_MUSIC_PAUSE = "pause music playing";
Blockly.Msg.CPEKU_MUSIC_PAUSE_TOOLTIP = "temporary pause current music playing";

Blockly.Msg.CPEKU_MUSIC_RESUME = "resume music playing";
Blockly.Msg.CPEKU_MUSIC_RESUME_TOOLTIP = "resume music playing from the last pause";

Blockly.Msg.CPEKU_MUSIC_CANCEL = "cancel music playing";
Blockly.Msg.CPEKU_MUSIC_CANCEL_TOOLTIP = "cancel current music playing and clear all notes";
