#ifndef __CPEKU_MUSIC_H__
#define __CPEKU_MUSIC_H__

#include <map>
#include <queue>
#include "driver.h"
#include "device.h"
#include "esp_log.h"
#include "pt/pt.h"

struct note_t {
  int freq;
  double duration;
  int velocity;

  note_t(int f, double d, int v):freq(f),duration(d),velocity(v) {};
};

class CPEKU_MMLParser {
public:
  typedef std::queue<note_t> notebuf_t;
  CPEKU_MMLParser();
  uint16_t parse(const char* seq, notebuf_t& notes);

private:
  std::map<char,int> _note_map;
  char _mode = 'n';
  int _octave = 4;
  int _divide = 4;
  int _tempo = 72;
  int _vel = 90;
  double _hold_duration(double duration);
};

class CPEKU_Music : public Device {
  public:
    CPEKU_Music();
    void init(void) override;
    void process(Driver *drv) override;
    int prop_count(void) override { return 0; }
    bool prop_name(int index, char *name) override { return false; }
    bool prop_unit(int index, char *unit) override { return false; }
    bool prop_attr(int index, char *attr) override { return false; }
    bool prop_read(int index, char *value) override { return false; }
    bool prop_write(int index, char *value) override { return false; }
    void play(const char* seq);
    bool is_playing();
    void wait_done();
    void pause();
    void resume();
    void cancel();

  private:
    enum state_t {
      DETECT,
      RUNNING,
    };
    static state_t _state;
    static CPEKU_MMLParser _mmlparser;
    static CPEKU_MMLParser::notebuf_t _notebuf;
    static bool _paused;
    PT_THREAD(_task_play(struct pt* pt));
};

#endif
